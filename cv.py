import cv2
import numpy as np
image = cv2.imread('source.jpg',cv2.IMREAD_COLOR)
gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imwrite('gray_image.jpg',gray_image)
cv2.equalizeHist(gray_image)
cv2.imwrite('gray_equalized.jpg',gray_image)
edges = cv2.Canny(gray_image,100,200)
gray_image = np.float32(gray_image)
corners = cv2.goodFeaturesToTrack(gray_image,170,0.01,10)
corners = np.int0(corners)
for corner in corners:
    x, y = corner.ravel()
    cv2.circle(edges, (x, y), 2, 255, -1)
cv2.imwrite('edges.jpg',edges)
dist = cv2.distanceTransform(255 - edges, cv2.DIST_L2, 3)
cv2.imwrite('dist.jpg',dist)
intergral_img = cv2.integral(image)
intergral_img = intergral_img[1:,1:]

    # smoothed_filter
h, w, ch = image.shape
for c in range(ch):
    for i in range(h):
        for j in range(w):
            kernel_size = int(round(dist[i][j]))
            if kernel_size % 2 == 0:
                kernel_size += 1
            if ((i <= kernel_size//2) or (j <= kernel_size//2) or (j > w - kernel_size//2 - 1)
                or (i > h - kernel_size//2 - 1) or (kernel_size <= 1)):
                continue
            _sum = (intergral_img[i-kernel_size//2-1][j-kernel_size//2-1][c]
                    - intergral_img[i-kernel_size//2-1][j+kernel_size//2][c]
                    + intergral_img[i+kernel_size//2][j+kernel_size//2][c]
                    - intergral_img[i+kernel_size//2][j-kernel_size//2-1][c])
                # whithout integral image
            # for l in range(i - kernel_size//2, i + kernel_size//2 + 1):
            #     for k in range(j - kernel_size//2, j + kernel_size//2 + 1):
            #         _sum += img[l][k][c]

            image[i][j][c] = _sum//(kernel_size**2)

cv2.imwrite('res.jpg',image)
cv2.namedWindow("res", cv2.WINDOW_NORMAL)
cv2.imshow("res", image)
cv2.waitKey(0)
cv2.destroyAllWindows()